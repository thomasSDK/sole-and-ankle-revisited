/* eslint-disable no-unused-vars */
import React from 'react';
import styled from 'styled-components/macro';
import { DialogOverlay, DialogContent } from '@reach/dialog';

import { QUERIES } from '../../constants';

import UnstyledButton from '../UnstyledButton';
import Icon from '../Icon';
import VisuallyHidden from '../VisuallyHidden';

const MobileMenu = ({ isOpen, onDismiss }) => {
  if (!isOpen) {
    return null;
  }

  return (
    <Overlay isOpen={isOpen} onDismiss={onDismiss}>
      <Content>
        <Close>
          <CloseButton id="close" onClick={onDismiss}>
            <Icon id="close" />
            <VisuallyHidden>Close</VisuallyHidden>
          </CloseButton>
        </Close>
        <Nav>
          <a href="/sale">Sale</a>
          <a href="/new">New&nbsp;Releases</a>
          <a href="/men">Men</a>
          <a href="/women">Women</a>
          <a href="/kids">Kids</a>
          <a href="/collections">Collections</a>
        </Nav>
        <Footer>
          <a href="/terms">Terms and Conditions</a>
          <a href="/privacy">Privacy Policy</a>
          <a href="/contact">Contact Us</a>
        </Footer>
      </Content>
    </Overlay>
  );
};

const Overlay = styled(DialogOverlay)`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: calc(var(--gray-500) + hsl(0,0%,0%, 0.5));
display: flex;
justify-content: flex-end;
`

const Content = styled(DialogContent)`
  position: relative;
  height: 100%;
  width: 300px;
  background-color: var(--white);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 32px 22px 32px 32px;

  a {
    text-decoration: none;

    &: hover {
      color: var(--secondary);
    }
  }
`

const Nav = styled.nav`
  display: flex;
  flex-direction: column;
  text-transform: uppercase;
  a {
    line-height: 2.5;
    color: var(--gray-900);
    font-weight: 600;
    font-size: ${18 / 16}rem;
  }
`

const Footer = styled.footer`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  flex: 1;

  a {
    line-height: 2;
    color: var(--gray-700);
    font-size: ${14 / 16}rem;
    font-weight: 500;
  }
`

const Close = styled.div`
  align-self: flex-end;
  flex: 1;
  transform: translate(16px, -16px);
`

const CloseButton = styled(UnstyledButton)`
  padding: 16px;
`

export default MobileMenu;
